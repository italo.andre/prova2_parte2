#!/bin/bash
opcao=""
arquivo=""
menu () {
while true $opcao != "0"
do
clear
echo "===== Menu ====="
echo "a - Selecionar um arquivo"
echo "b - Fazer um preview do arquivo selecionado"
echo "c - Exibir o arquivo selecionado"
echo "d - Criar um arquivo com N linhas de blablabla"
echo "e - Pesquisar algo no arquivo selecionado"
echo "f - Criar uma copia do arquivo selecionado"
echo "g - Sair"
echo "Digite a opcao desejada:"
read opcao

case "$opcao" in
  a)
    clear
    echo "Informe o caminho do arquivo:"
    read arquivo
    echo $arquivo
    sleep 10
  ;;
  b)
    clear
    echo "Preview do arquivo selecionado..."
    if [ -e "$arquivo" ] ; then
      cat $arquivo
    else
      echo "O arquivo nao existe..."
    fi
    sleep 5
  ;;
  c)
    clear
    echo "Exibindo o arquivo selecionado..."
    cat $arquivo | more
    sleep 5
  ;;
  d)
    clear
    echo "Criar arquivo com N linhas de blablabla..."
    echo "Informe a quantidade de linhas: "
    read linhas
    inicial="\n"
    for counter in $(seq 1 $linhas);
    do
      inicial="${inicial} blablabla \n";
    done;
    printf $inicial >> arquivo-gerado.txt
    sleep 5
  ;;
  e)
    clear
    echo "Informe o que deseja pesquisar:"
    read pesquisa
    grep $pesquisa $arquivo
    echo "Pesquisa finalizada..."
    sleep 5
  ;;
  f)
    clear
    echo "Gerando copia do arquivo..."
    echo "Informe o nome da copia: "
    read copia
    cp $arquivo $copia
    echo "Arquivo copiado com sucesso..."
    sleep 5
  ;;
  g)
    echo "Saindo..."
    clear;
    exit;
  ;;
  *)
    echo "Opcao invalida!!"
    sleep 5
esac
done
}
menu